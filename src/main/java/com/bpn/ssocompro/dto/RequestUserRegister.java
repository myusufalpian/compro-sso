package com.bpn.ssocompro.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestUserRegister {
    private String userEmail;
    private String userFirstName;
    private String userLastName;
    private String userPhone;
    private String userPass;
    private String userConfirmPass;
}
