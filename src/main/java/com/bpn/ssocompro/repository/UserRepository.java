package com.bpn.ssocompro.repository;

import com.bpn.ssocompro.model.User;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findUserByUserEmailAndUserStatus(String email, Integer userStatus);

    Optional<User> findUserByUserUuidAndUserStatus(String userUuid, Integer userStatus);

    Collection<User> findUserByUserStatus(Integer userStatus);
}
