package com.bpn.ssocompro.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bpn.ssocompro.model.UserOtp;

public interface UserOtpRepository extends JpaRepository<UserOtp, Integer> {
    Optional<UserOtp> findUserOtpByUserOtpCodeAndUserOtpStatus(String userOtpCode, Integer userOtpStatus);
}
