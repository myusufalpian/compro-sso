package com.bpn.ssocompro.utility;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Helper {
    public static String generateOTP(Integer length) {
        StringBuilder otp = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int digit = random.nextInt(10); // Generate a random digit between 0 and 9
            otp.append(digit);
        }
        return otp.toString();
    }

    public static Date addTimes(Date date, String type, Integer times) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (type.equalsIgnoreCase("minutes")) {
            calendar.add(Calendar.MINUTE, times);
        } else if (type.equalsIgnoreCase("hours")) {
            calendar.add(Calendar.HOUR, times);
        } else if (type.equalsIgnoreCase("days")) {
            calendar.add(Calendar.DAY_OF_MONTH, times);
        }
        return calendar.getTime();
    }

    public static Integer checkUpperCase(String text) {
        Integer result = 0;
        char ch;
        for(int i=0; i < text.length(); i++) {
            ch = text.charAt(i);
            if (Character.isUpperCase(ch)) {
                result = result + 1;
            }
        }
        return result;
    }

    public static Integer checkNumericCase(String text) {
        Integer result = 0;
        char ch;
        for(int i=0; i < text.length(); i++) {
            ch = text.charAt(i);
            if (Character.isDigit(ch)) {
                result = result + 1;
            }
        }
        return result;
    }

    public static Integer checkSpecialChars(String text) {
        Integer result = 0;
        String specialChars = "~`!@#$%^&*()-_=+\\|[{]};:'\",<.>/?";
        char ch;
        for(int i=0; i < text.length(); i++) {
            ch = text.charAt(i);
            if (specialChars.contains(String.valueOf(ch))) {
                result = result + 1;
            }
        }
        return result;
    }
}
