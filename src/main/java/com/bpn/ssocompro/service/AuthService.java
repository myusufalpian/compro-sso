package com.bpn.ssocompro.service;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bpn.ssocompro.config.JwtUtil;
import com.bpn.ssocompro.dto.RequestUserRegister;
import com.bpn.ssocompro.dto.UserDetailDTO;
import com.bpn.ssocompro.dto.UserRequestLogin;
import com.bpn.ssocompro.model.User;
import com.bpn.ssocompro.model.UserOtp;
import com.bpn.ssocompro.repository.UserOtpRepository;
import com.bpn.ssocompro.repository.UserRepository;
import com.bpn.ssocompro.utility.GenerateResponse;
import com.bpn.ssocompro.utility.Helper;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.transaction.Transactional;

@Service
public class AuthService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserOtpRepository userOtpRepository;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ModelMapper mapper;

    @Autowired
    NativeQueryService nativeQueryService;

    @Autowired
    private MailSender mailSender;

    public ResponseEntity<String> login(UserRequestLogin param) throws JsonProcessingException{
        if(param.getUsername().equals("") || param.getUsername() == null) return GenerateResponse.badRequest("Username cannot be null", null);
        else if (param.getPassword().equals("") || param.getPassword() == null) return GenerateResponse.badRequest("Password cannot be null", null);    
        
        Optional<User> user = userRepository.findUserByUserEmailAndUserStatus(param.getUsername(), 0);
        if (!user.isPresent()) return GenerateResponse.notFound("User not found", null);
        if (!encoder.matches(param.getPassword(), user.get().getUserPass())) return GenerateResponse.badRequest("Username / Password not matched", null);
        return GenerateResponse.success("Login Success", jwtUtil.generate(param.getUsername(), "ACTIVE"));
    }

    public ResponseEntity<String> getAllUser() throws JsonProcessingException{
        Collection<User> users = userRepository.findUserByUserStatus(0);
        return GenerateResponse.success("Get data success", users.stream().map(x -> mapper.map(x, UserDetailDTO.class)).collect(Collectors.toList()));
    }

    @Transactional
    public ResponseEntity<String> register(RequestUserRegister param) throws JsonProcessingException{
        if(param.getUserEmail().equals("") || param.getUserEmail() == null) {
            return GenerateResponse.badRequest("Username cannot be null", null);
        } else if (param.getUserPass().equals("") || param.getUserPass() == null) {
            return GenerateResponse.badRequest("Password cannot be null", null);
        } else if (!param.getUserPass().equals(param.getUserConfirmPass())) {
            return GenerateResponse.badRequest("Password and confirmation password not matched", null);
        }

        Optional<User> checkUser = userRepository.findUserByUserEmailAndUserStatus(param.getUserEmail(), 0);
        if(!checkUser.isPresent()) {
            return GenerateResponse.badRequest("Email already taken", null);
        } else if(Helper.checkNumericCase(param.getUserPass()) < 2) {
            return GenerateResponse.badRequest("Password must have at least 2 numeric letters", null);
        } else if(Helper.checkUpperCase(param.getUserPass()) < 2) {
            return GenerateResponse.badRequest("Password must have at least 2 upper case letters", null);
        } else if(Helper.checkSpecialChars(param.getUserPass()) < 2) {
            return GenerateResponse.badRequest("Password must have at least 2 special characters", null);
        }

        User user = mapper.map(param, User.class);
        user.setUserPass(encoder.encode(param.getUserPass()));
        user.setCreatedBy(0);
        user.setUserStatus(2);
        userRepository.save(user);

        UserOtp userOtp = new UserOtp();
        userOtp.setCreatedBy(user.getUserId());
        userOtp.setUserId(user.getUserId());
        userOtp.setCreatedDate(new Date());
        userOtp.setUserOtpExp(Helper.addTimes(userOtp.getCreatedDate(), "minutes", 3));
        userOtp.setUserOtpCode(Helper.generateOTP(6));
        userOtp.setUserOtpStatus(0);
        userOtpRepository.save(userOtp);

        sendEmail(param.getUserEmail(), "OTP BPP Company Profile", 
            "The following is the verification code that can be used to login to BPN Company Profile \n OTP : " + userOtp.getUserOtpCode() + "\n The code above is only valid for 3 minutes. Do not tell the code to anyone, include BPN Company Profile");
        return GenerateResponse.success("Register success, please check your email for confirmation your registration", null);
    }

    @KafkaListener(topics = "otp", groupId = "sso-compro")
    public void sendEmail(String to, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
}
