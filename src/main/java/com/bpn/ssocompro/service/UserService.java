package com.bpn.ssocompro.service;

import com.bpn.ssocompro.config.JwtUtil;
import com.bpn.ssocompro.dto.UserDetailDTO;
import com.bpn.ssocompro.model.User;
import com.bpn.ssocompro.repository.UserRepository;
import com.bpn.ssocompro.utility.GenerateResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    ModelMapper mapper;

    public ResponseEntity<String> getDetailUser(String userUuid) throws JsonProcessingException {
        Optional<User> user = userRepository.findUserByUserUuidAndUserStatus(userUuid, 0);
        UserDetailDTO userDetail = mapper.map(user.get(), UserDetailDTO.class);
        return (!user.isPresent()) ? GenerateResponse.notFound("User not found", null) : GenerateResponse.success("User not found", userDetail);
    }
}
