package com.bpn.ssocompro.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.bpn.ssocompro.model.User;
import com.bpn.ssocompro.model.UserOtp;
import com.bpn.ssocompro.repository.UserOtpRepository;
import com.bpn.ssocompro.repository.UserRepository;
import com.bpn.ssocompro.utility.GenerateResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.transaction.Transactional;

@Service
public class UserOtpService {

    @Autowired
    private UserOtpRepository userOtpRepository;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public ResponseEntity<String> checkOtp(String otp) throws JsonProcessingException {
        Optional<UserOtp> userOtp = userOtpRepository.findUserOtpByUserOtpCodeAndUserOtpStatus(otp, 0);
        if(!userOtp.isPresent()) {
            return GenerateResponse.notFound("User otp not found", null);
        } else if(userOtp.get().getUserOtpExp().before(new Date())) {
            userActivate(2, 1, userOtp.get());
            return GenerateResponse.badRequest("Otp was expired", null);
        }
        userActivate(1, 0, userOtp.get());
        return GenerateResponse.success("User activated success", null);
    }

    private void userActivate(Integer otpStatus, Integer userStatus, UserOtp userOtp) {
        userOtp.setUserOtpStatus(otpStatus);
        userOtp.setUpdatedBy(userOtp.getUserId());
        userOtp.setUpdatedDate(new Date());
        userOtpRepository.save(userOtp);

        Optional<User> user = userRepository.findById(userOtp.getUserId());
        user.get().setUserStatus(userStatus);
        user.get().setUpdatedDate(new Date());
        user.get().setUpdatedBy(user.get().getUserId());
        userRepository.save(user.get());
    }

}
