package com.bpn.ssocompro.service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NativeQueryService {
    @PersistenceContext
    private EntityManager entityManager;

    public List<HashMap<String, Object>> getResultList(String sql, Map<String, Object> param) {
        try {
            Session session = entityManager.unwrap(Session.class);
            return session.unwrap(Session.class).createQuery(sql).setProperties(param).setResultTransformer(Transformers.aliasToBean(Session.class)).list();
        } catch (Exception e) {
            System.out.println(sql);
            e.getLocalizedMessage();
            throw new RuntimeException(e);
        }
    }
}
