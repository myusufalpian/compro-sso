package com.bpn.ssocompro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bpn.ssocompro.service.UserOtpService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping(value = "/otp", produces= MediaType.APPLICATION_JSON_VALUE)
public class UserOtpController {

    @Autowired
    private UserOtpService userOtpService;

    @GetMapping(value = "/validate")
    public ResponseEntity<String> checkOtp(@RequestParam(value = "code", required = true) String otp) throws JsonProcessingException {
        return userOtpService.checkOtp(otp);
    }
}
