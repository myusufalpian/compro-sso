package com.bpn.ssocompro.controller;

import com.bpn.ssocompro.dto.RequestUserRegister;
import com.bpn.ssocompro.dto.UserRequestLogin;
import com.bpn.ssocompro.service.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/auth", produces= MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping(value = "/login")
    public ResponseEntity<String> login(@RequestBody UserRequestLogin param) throws JsonProcessingException {
        return authService.login(param);
    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<String> getAll() throws JsonProcessingException {
        return authService.getAllUser();
    }

    @PostMapping(value = "/register")
    public ResponseEntity<String> register(@RequestBody RequestUserRegister param) throws JsonProcessingException {
        return authService.register(param);
    }

}
